package ex1;


import math.geom2d.Point2D;
import math.geom2d.line.LineSegment2D;
import math.geom2d.line.StraightLine2D;

import java.util.*;

public class VisDetector {

    private static final Point2D CAM_POINT = new Point2D(0,0);
    private LineSegment2D lsRed;
    private LineSegment2D lsBorder1;
    private LineSegment2D lsBorder2;

    public boolean redIsVisible(List<Point2D> points){
        initMainLineSegments(points);
        List<LineSegment2D> blackLines = getBlackLines(points);

        if (!checkMainAngleIsVisible(blackLines)) return false;
        List<Point2D> redIntersactions = checkIntersactionsWithRedLine(blackLines);
        List<Point2D> poitsInside = findPointsInside(points, redIntersactions);
        if (poitsInside.isEmpty()) return true;
        List<LineSegment2D[]> subsAngles = buildSubsAngles(poitsInside);
        return checkAnglesIntersactions(blackLines, subsAngles);
    }


    private void initMainLineSegments(List<Point2D> points){
        Point2D redP1 = points.remove(points.size()-2);
        Point2D redP2 = points.remove(points.size()-1);
        lsBorder1 = new LineSegment2D(CAM_POINT, redP2);
        lsRed = new LineSegment2D(redP2, redP1);
        lsBorder2 = new LineSegment2D(redP1, CAM_POINT);
    }

    private List<LineSegment2D> getBlackLines(List<Point2D> points){
        List<LineSegment2D> blackLines = new ArrayList<LineSegment2D>();
        for (int i = 0; i < points.size() ; i+=2) {
            blackLines.add(new LineSegment2D(points.get(i), points.get(i+1)));
        }
        return blackLines;
    }

    private boolean checkVertical(LineSegment2D line){
        if (line.firstPoint().x() - line.lastPoint().x() == 0) return true;
        return false;
    }


    private List<Point2D> findPointsInside(List<Point2D> points, List<Point2D> pointsOnRed){
        List<Point2D> pointsInside = new ArrayList<Point2D>();
        for (Point2D p : points){
            boolean t1 = lsBorder1.isInside(p);
            boolean t2 = lsBorder2.isInside(p);
            boolean t3 = lsRed.isInside(p);
            if (lsBorder1.isInside(p) && lsBorder2.isInside(p) && (lsRed.isInside(p) || lsRed.contains(p))) pointsInside.add(p);
        }
        if (!pointsOnRed.isEmpty() && !checkVertical(lsRed) ) pointsInside.addAll(pointsOnRed);
        pointsInside = sortPoints(pointsInside);
        return pointsInside;
    }

    private List<Point2D> checkIntersactionsWithRedLine(List<LineSegment2D> blackLines){
        List<Point2D> foundPoints = new ArrayList<>();
        for (LineSegment2D l : blackLines){
            Point2D p = l.intersection(lsRed);
            if (p!=null) foundPoints.add(p);
        }
        return foundPoints;
    }

    private List<LineSegment2D[]> buildSubsAngles(List<Point2D> pointsInside){
        List<LineSegment2D[]> subsAngles = new ArrayList<LineSegment2D[]>();
        LineSegment2D prevLine = lsBorder2;
        for (Point2D p : pointsInside){
            StraightLine2D tempLine = new StraightLine2D(CAM_POINT, p);
            Point2D redP = tempLine.intersection(lsRed);
            LineSegment2D foundLs = new LineSegment2D(CAM_POINT, redP);
            LineSegment2D[] angle = {prevLine, foundLs};
            subsAngles.add(angle);
            prevLine = foundLs;
        }
        if (!subsAngles.isEmpty()){
            LineSegment2D ls = subsAngles.get(subsAngles.size()-1)[1];
            LineSegment2D[] lastAngle = {ls, lsBorder1};
            subsAngles.add(lastAngle);
        }
        return subsAngles;
    }

    private boolean checkMainAngleIsVisible(List<LineSegment2D> blackLines){
        for (LineSegment2D l : blackLines){
            if (!angleIsVisible(lsBorder1, lsBorder2, l)) return false;
        }
        return true;
    }


    private boolean checkAnglesIntersactions(List<LineSegment2D> blackLines, List<LineSegment2D[]> subsAngles){
        Set<LineSegment2D[]> unvisAngles = new HashSet<>();
        for (LineSegment2D bl : blackLines){
            for (LineSegment2D[] angle : subsAngles){
                boolean isVis = angleIsVisible(angle[0], angle[1], bl);
                if (!isVis) unvisAngles.add(angle);
            }
        }

        if (unvisAngles.size() < subsAngles.size()) return true;
        return false;
    }

    private boolean angleIsVisible(LineSegment2D border1, LineSegment2D border2, LineSegment2D blackLine){
        Point2D p = blackLine.intersection(border1);
        Point2D p2 = blackLine.intersection(border2);
        if (p != null && p2 !=null) return false;
        return true;
    }

    private List<Point2D> sortPoints(List<Point2D> points){
        points.sort(new Comparator<Point2D>() {
            public int compare(Point2D p1, Point2D p2) {
                Double distance1 = lsBorder2.distance(p1);
                Double distance2 = lsBorder2.distance(p2);
                return distance1.compareTo(distance2);
            }
        });
        return points;
    }

}
