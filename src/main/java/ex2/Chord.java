package ex2;

public class Chord {

    private double[] point;
    private double[] point1;


    public Chord(double[] point, double[] point1) {
        this.point = point;
        this.point1 = point1;
    }

    public double[] getPoint() {
        return point;
    }

    public void setPoint(double[] point) {
        this.point = point;
    }

    public double[] getPoint1() {
        return point1;
    }

    public void setPoint1(double[] point1) {
        this.point1 = point1;
    }

    public boolean equals(Chord another){
        if (point[0] == another.getPoint()[0] &&
                point[1] == another.getPoint()[1] &&
                point1[0] == another.getPoint1()[0] &&
                point1[1] == another.getPoint1()[1]){
            return true;
        }
        return false;
    }

}
