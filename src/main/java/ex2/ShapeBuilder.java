package ex2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ShapeBuilder {

    private double angle;
    private double extraAngle;

    private double angle360 = 6.28318;

    private double summaryAngle;
    private List<Chord> foundChords;


    public List<Chord> designShape(double radius, double chord){
        foundChords = new ArrayList<>();
        angle = findAngle(radius, chord);
        extraAngle = findAngle(radius, chord * 0.2);
        while (summaryAngle<=angle360){
            Chord chord1 = buildChord(radius);
            if(chord1!=null) {foundChords.add(chord1);}
            else break;
            summaryAngle += extraAngle;
        }
        return foundChords;
    }

    private Chord buildChord(double radius){
        double[] firstPoint = setPoint(radius);
        if (summaryAngle + angle > angle360 ) return null;
        summaryAngle += angle;
        double[] secondPoint = setPoint(radius);
        return new Chord(firstPoint, secondPoint);
    }

    private double [] setPoint(double radius){
        double x1 = radius * Math.cos(summaryAngle);
        double y1 = radius * Math.sin(summaryAngle);
        double[] point = {new BigDecimal(x1).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue(), new BigDecimal(y1).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue()};
        return point;
    }

    private double findAngle(double radius, double chord){
        double cosAngle = ((2 * Math.pow(radius,2)) - (Math.pow(chord,2))) / (2 * Math.pow(radius,2)) ;
        double radAngle = Math.acos(cosAngle);
        return radAngle;
    }

}
