import ex2.Chord;
import ex2.ShapeBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShapeBuilderTest {

    private ShapeBuilder shapeBuilder = new ShapeBuilder();

    @Test
    public void checkResult1(){
        List<Chord> chords = shapeBuilder.designShape(3,6);
        for (int i = 0; i < chords.size() ; i++) {
            Assert.assertTrue(chords.get(i).equals(chords1.get(i)));
        }
    }


    @Test
    public void checkResult2(){
        List<Chord> chords = shapeBuilder.designShape(3,5);
        for (int i = 0; i < chords.size() ; i++) {
            Assert.assertTrue(chords.get(i).equals(chords2.get(i)));
        }
    }


    @Test
    public void checkResult3(){
        List<Chord> chords = shapeBuilder.designShape(3,4);
        for (int i = 0; i < chords.size() ; i++) {
            Assert.assertTrue(chords.get(i).equals(chords3.get(i)));
        }
    }


    private double[] a1 = {3,0};
    private double[] b1 = {-3,0};

    private double[] a2 = {3,0};
    private double[] b2 = {-1.2, 2.8};
    private double[] a3 = {-2, 2.2};
    private double[] b3 = {-1.3, -2.7};

    private double[] a4 = {3,0};
    private double[] b4 = {0.3, 3};
    private double[] a5 = {-0.5, 3};
    private double[] b5 = {-3, -0.1};
    private double[] a6 = {-2.9, -0.9};
    private double[] b6 = {0.6, -2.9};


    private Chord chord1 = new Chord(a1, b1);
    private Chord chord2 = new Chord(a2, b2);
    private Chord chord3 = new Chord(a3, b3);
    private Chord chord4 = new Chord(a4, b4);
    private Chord chord5 = new Chord(a5, b5);
    private Chord chord6 = new Chord(a6, b6);

    private List<Chord> chords1 = new ArrayList<>();
    private List<Chord> chords2 = new ArrayList<>();
    private List<Chord> chords3 = new ArrayList<>();


    {
        chords1.add(chord1);
        chords2.add(chord2);
        chords2.add(chord3);
        chords3.add(chord4);
        chords3.add(chord5);
        chords3.add(chord6);
    }



}
