import ex1.VisDetector;
import math.geom2d.Point2D;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class VisDetectorTest {

    private VisDetector visDetector = new VisDetector();

    @Test
    public void checkVisibility1(){
        boolean result = visDetector.redIsVisible(testData1);
        Assert.assertFalse(result);
    }

    @Test
    public void checkVisibility2(){
        boolean result = visDetector.redIsVisible(testData2);
        Assert.assertFalse(result);
    }

    @Test
    public void checkVisibility3(){
        boolean result = visDetector.redIsVisible(testData3);
        Assert.assertTrue(result);
    }

    @Test
    public void checkVisibility4(){
        boolean result = visDetector.redIsVisible(testData4);
        Assert.assertFalse(result);
    }

    @Test
    public void checkVisibility5(){
        boolean result = visDetector.redIsVisible(testData5);
        Assert.assertTrue(result);
    }

    @Test
    public void checkVisibility6(){
        boolean result = visDetector.redIsVisible(testData6);
        Assert.assertFalse(result);
    }



    private final List<Point2D> testData1 = new ArrayList<>();
    private final List<Point2D> testData2 = new ArrayList<>();
    private final List<Point2D> testData3 = new ArrayList<>();
    private final List<Point2D> testData4 = new ArrayList<>();
    private final List<Point2D> testData5 = new ArrayList<>();
    private final List<Point2D> testData6 = new ArrayList<>();





    {
        testData1.add(new Point2D(-2,2));
        testData1.add(new Point2D(2,2));
        testData1.add(new Point2D(-3,3));
        testData1.add(new Point2D(3,3));


        testData2.add(new Point2D(-3,2));
        testData2.add(new Point2D(3,2));
        testData2.add(new Point2D(-3,3));
        testData2.add(new Point2D(3,3));


        testData3.add(new Point2D(2,2));
        testData3.add(new Point2D(6,2));
        testData3.add(new Point2D(-2,4));
        testData3.add(new Point2D(2,4));
        testData3.add(new Point2D(-3,3));
        testData3.add(new Point2D(2,3));
        testData3.add(new Point2D(1,5));
        testData3.add(new Point2D(5,3));


        testData4.add(new Point2D(-1,2));
        testData4.add(new Point2D(2,2));
        testData4.add(new Point2D(-3,1));
        testData4.add(new Point2D(2,6));
        testData4.add(new Point2D(-3,3));
        testData4.add(new Point2D(3,5));


//        red vertical

        testData5.add(new Point2D(-3,3));
        testData5.add(new Point2D(3,3));
        testData5.add(new Point2D(0,2));
        testData5.add(new Point2D(0,6));


        testData6.add(new Point2D(-3,2));
        testData6.add(new Point2D(3,2));
        testData6.add(new Point2D(0,2));
        testData6.add(new Point2D(0,6));


    }


}
